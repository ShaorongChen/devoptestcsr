# DevOpTestCSR

**Gov Tech DevOps 001**


1. Prerequisite
----------------
Install docker to your linux machine using the following command:

```bash
curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh
```

This is the simplest way and it will install the new version to the local machine, ubuntu, centos, debian, etc.

Install docker compose to your linux machine using the following command:

```bash
curl -SL "https://github.com/docker/compose/releases/download/<theVersion>/docker-compose-$(uname -s)-$(uname -m)" -o ~/.docker/cli-plugins/docker-compose
```

Example:
```bash
curl -SL "https://github.com/docker/compose/releases/download/v2.1.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose && docker-compose version
```

Replace <theVersion> with the latest stable releases number from git hub, https://github.com/docker/compose/releases


2. Usage
---------

To test  docker image, run the following command:

```bash
make docker_image;
make docker_testrun;
```

Or you are advance docker user the following command will perfom the same function, do nagivate to the root folder of the git clone:

```bash
docker build --rm -f ./deployments/build/Dockerfile -t devops/pinger:latest .
docker run --rm -it -p 8000:8000 devops/pinger:latest
```

Changing ENV variable in the dockerfile will allow you to ping other host else removing the variable will trigger the container to ping itself:

```yaml
ENV TARGET_PROTO https
ENV TARGET_HOST www.csral.net
ENV TARGET_PORT 443
ENV TARGET_PATH /
```

This is currently set to ping my test web site that is hosted in my Pi4, nginx which is behind Cloudflare DDoS and Bot Protection.

To test docker compose, run the following command:
```bash
make testenv;
```

Or you are advance docker user the following command will perfom the same function, do nagivate to the root folder of the git clone:
```bash
docker-compose -f ./deployments/docker-compose.yml up #this will show both container logs Ctrl C to stop
or
docker-compose -f ./deployments/docker-compose.yml up -d #in detach mode
docker container logs <container ID or Name>
docker-compose -f ./deployments/docker-compose.yml down -v #remove any volume
```

Changing ENV variable in the dockerfile will allow you to ping other host or if you wish to change the service names:

```yaml
networks:
      - frontend
environment:
      - TARGET_PROTO=http
      - TARGET_HOST=<theHost>
      - TARGET_PORT=8000
      - TARGET_PATH=/
...
networks:
  frontend:
```

An example if the service names is the following:

```yaml
services:
  ping1:

...

services:
  ping2:
```

In order for _ping2_ service to ping _ping1_ set the environment variable to:

```yaml
environment:
      - TARGET_PROTO=http
      - TARGET_HOST=ping1
      - TARGET_PORT=8000
      - TARGET_PATH=/
```

Docker compose will create a default network with the folder name that is is ran from, example, _deployments_network_.  Docker will take care of the DNS resolution. The docker compose file contain syntax declaration for _networks_ that will work in both docker swarm stack **20.10.10** and standard docker compose.

To initialise and build a 3 node docker swarm do follow the documentation from docker, https://docs.docker.com/engine/swarm/swarm-tutorial/. You could create _frontend_ manully if you wish to do so by using the following command when you have swarm initialised:

```bash
docker network create -d overlay frontend && docker network ls
```

3. CI Build Cycle
------------------
I have added addition to push the docker image to a public docker hub (normally I keep my images in private docker hub) so that you can see step beside tar image. Using the following variable in my Setting ---> CI/CD ---> Variables (Protected and Masked)

- CI_REGISTRY
- CI_REGISTRY_IMAGE
- CI_REGISTRY_USER
- CI_REGISTRY_PASSWORD

This is not the best practice as I do not have a hashicorp vault to intergate to like I normally do with on premise Jenkin.

Thank you and have a great day! Logging off... Shaorong Chen


